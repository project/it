* Progetto di traduzione di Drupal in Italiano *

by MicheleF - michele@drupalitalia.org
Visitate il nostro sito http://www.drupalitalia.org

** aggiornamento al 26/1/06 **

La presente traduzione � ora aggiornata a Drupal 4.7b 4. Il metodo di importazione della traduzione non � variato.

** aggiornamento al 25/3/05 **

La presente traduzione � ora aggiornata a Drupal 4.6 Release Candidate. Alcune informazioni contenute in questo file readme potrebbero essere obsolete. Il metodo di importazione della traduzione non � invece variato.


** 29/11/04 **

Lo scopo del presente progetto � realizzare una traduzione in italiano di Drupal 4.5. 

Al momento della scrittura di questo readme, la traduzione � stata adattata all'ultima versione stabile di Drupal (Drupal 4.5) ed � completa per circa l'85% dei moduli principali (27 moduli completamente tradotti, 5 completi a parte l'help). 

La traduzione pu� essere utilizzata regolarmente ma potrebbe contenere alcune imprecisioni.

NOTA IMPORTANTE: TUTTE LE TRADUZIONI SONO FORNITE AS IS E NON SI RISPONDE DI EVENTUALI MALFUNZIONAMENTI, ERRORI O DANNI CAUSATI DA CATTIVE TRADUZIONI

Si prega di segnalare eventuali imprecisioni riscontrate tramite il forum di Drupal Italia:

http://www.drupalitalia.org/?q=forum

Specificare l'errore trovato, e dove possibile il modulo a cui si riferisce e la traduzione suggerita.


-- nota sulla realizzazione delle traduzioni --


le traduzioni sono realizzate in modo modulare. I singoli moduli xxx-module.po contenenti ciascuno la traduzione di un singolo modulo Drupal, vengono composti attraverso uno script automatico nel file complessivo it.po, che � quello destinato all'utente finale. E' possibile trovare l'ultima versione del file it.po al link 

http://drupal.org/files/projects/it-cvs.tar.gz




*** Come utilizzare la traduzione (se hai fretta leggi qui) ***


Dopo avere installato o aggiornato Drupal secondo quanto descritto nel manuale di installazione o nel file install.txt di Drupal, siete pronti a inserire la traduzione in lingua italiana.


-- dove trovare il file di traduzione --

La traduzione viene fornita sotto forma di file it.po, disponibile al download al seguente link:

http://drupal.org/project/it

La versione CVS � pi� aggiornata e tendenzialmente pi� corretta dell'ultima release.


-- come attivare le traduzioni -- 

Per utilizzare Drupal in una lingua diversa dall'inglese, � necessario attivare il modulo "locales":

1. Apri in un browser il tuo sito Drupal e autenticati come amministratore (si suppone che tu abbia gi� creato il primo utente come da manuale di installazione)

2. All'interno del men� di navigazione,  selezionare "administer -> modules" per ottenere l'elenco dei moduli attivi

3. Attivare il modulo "locales" e salvare le impostazioni

4. Nel men� di navigazione compare ora la nuova voce "administer -> localization". Selezionarla per accedere alla pagina di configurazione delle lingue

5. Selezionare "add language", scorrere la tendina per selezionare la lingua italiana, e confermare con il tasto "Add Language"

6. E' possibile finalmente importare il file di traduzione. Selezionare "import", verificare di stare importando nella lingua italiana, quindi caricare il file it.po e dare conferma. Dopo qualche secondo, il sistema dar� conferma dell'avvenuta importazione.

7. Impostare l'italiano come lingua predefinita: seleziona "list" e seleziona le finestrelle "enabled" per abilitarlo, "default" per renderlo la lingua predefinita.


*** Note per i traduttori ***


-- come funziona la traduzione --


I programmi e le procedure per la traduzione sono documentate al link seguente:

http://drupal.org/node/11130


-- Nota speciale sui plurali con poEdit --


Per fare s� che poEdit gestisca correttamente i plurali, aprire "Catalogo > Ipostazioni" e aggiungere a "Forme di Plurale" la seguente riga:

Forme di plurale: nplurals=2; plural=n != 1;


-- Suggerimenti di traduzione --


. restare pi� fedeli possibili all'originale
. cercare di fare frasi brevi purch� comprensibili (es. 'View posts that are new or updated' = 'mostra i messaggi nuovi o aggiornati' eliminando il 'che sono')
. non tradurre i termini tecnici (workflow, blog, forum, rss, etc.)
. utilizzare termini italiani ovunque possibile (feed, login, username = fonte, accedi, nome utente)


-- Termini suggeriti --


Per contribuire alla traduzione e mantenerne la coerenza, visitate i suggerimenti contenuti in http://www.drupalitalia.org
